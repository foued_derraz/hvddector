#include <iostream>

#include "HVDlib/ExportInterface/include/ExportInterface.h"

using namespace std;
using namespace vd;



int main (void)
{
    HVD hhvd;

    const std::string fileName("TestVideo.avi");
    const std::string modelPath("./models");
    const unsigned int shotInterval(100);
    const unsigned int sceneShotNum(20);

    unsigned int num =
        hhvd.HVDSceneNum(fileName.c_str(), shotInterval, sceneShotNum);

    double* resultSet = new double[num];

    if (hhvd.HVDDetector(resultSet, fileName.c_str(), modelPath.c_str(),
            shotInterval, sceneShotNum))
    {
        for (unsigned int i=0; i<num; ++i) {
            std::cout << " Scene[" << i << "] Result : " << resultSet[i] << std::endl;
        }
    } else {
        std::cout << " Failed to detect the video! " << std::endl;
    }
    delete[] resultSet;
	return 0;
}

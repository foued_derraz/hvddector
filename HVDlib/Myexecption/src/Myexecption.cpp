#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <memory>
#include <ctime>
#include <cstring>

#include "../../Myexecption/include/Myexecption.h"

using namespace std;

void throwDerivedException(string execptionMessage)

{
    cout << "throwDerivedException - thrown a derived exception" << endl;
    //string execptionMessage("MyException thrown");
    throw (MyException(execptionMessage));
}

void CatchDerivedException(string execptionMessage)
{
    cout << "illustrateDerivedExceptionsCatch - start" << endl;
    try
    {
        throwDerivedException(execptionMessage);
    }
    catch (const exception& e)
    {
        cout << "illustrateDerivedExceptionsCatch - caught an std::exception, e.what:" << e.what() << endl;
        // some additional code due to the fact that std::exception was thrown...
    }
    catch(const MyException& e)
    {
        cout << "illustrateDerivedExceptionsCatch - caught an MyException, e.what::" << e.what() << endl;
        // some additional code due to the fact that MyException was thrown...
    }

    cout << "illustrateDerivedExceptionsCatch - end" << endl;
}




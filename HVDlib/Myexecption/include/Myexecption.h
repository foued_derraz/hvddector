#ifndef MYEXECPTION_H_
#define MYEXECPTION_H_

#include <iostream>
#include <string>

using namespace std;

class MyException : public exception
{
public:
    MyException(const string& msg) : m_msg(msg)
    {
        cout << "MyException::MyException - set m_msg to:" << m_msg << endl;
    }

   ~MyException()
   {
        cout << "MyException::~MyException" << endl;
   }

   virtual const char* what() const throw ()
   {
        cout << "MyException - what" << endl;
        return m_msg.c_str();
   }

   const string m_msg;
};

void throwDerivedException(string execptionMessage);
void CatchDerivedException(string execptionMessage);

#endif /* EXPORTINTERFACE_H_ */

#ifndef _CLASSIFIER_H_
#define _CLASSIFIER_H_

#include <iostream>
#include <string>
#include <vector>

#include <stdexcept>
#include <fstream>
#ifdef __linux__
#include <unistd.h>
#endif

#include <opencv2/opencv.hpp>

namespace vd
{

	class Classifier 
	{

	public:

		Classifier(
            const cv::Mat& _videoFeature, const std::string& _modelPath);

		virtual ~Classifier(void);

	public:

        void setVideoFeature (const cv::Mat& _videoFeature);

        void setModelPath (const std::string& _modelPath);

	public:
		
        virtual const double calculateResult (void) = 0;

	protected:

        cv::Mat m_videoFeature;

        std::string m_modelPath;

	protected:

        virtual const double _predictValue (void) const = 0;

        virtual void _initModel (void) = 0;

        inline std::ostream& __printLog (std::ostream& _os, const std::string& _msg) const;

	};
}

# endif

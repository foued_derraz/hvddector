#include "../../Classifier/include/Classifier.h"
#include "../../Myexecption/include/Myexecption.h"

using namespace std;
using namespace cv;
using namespace vd;

/********************************************************************************/
Classifier::Classifier (
    const cv::Mat& _videoFeature, const std::string& _modelPath) :
    m_videoFeature(_videoFeature), m_modelPath(_modelPath)
{
	if (m_videoFeature.empty()) {
		__printLog (std::cerr, "Sorry, video feature is empty! ");
        throwDerivedException("Video Feature Error");
	}

 #ifdef __linux__
    if (access(m_modelPath.c_str(), 0)) {
		__printLog (std::cerr, "Sorry, the model path doesn't exist! ");
#endif
        throwDerivedException("Model Path Error");
	}

	return;
}

/********************************************************************************/

Classifier::~Classifier (void)
{
	return;
}

/*
*******************************************************************************/

void Classifier::setVideoFeature (const cv::Mat& _videoFeature)
{
	m_videoFeature = _videoFeature;

	if (m_videoFeature.empty()) {
		__printLog (std::cerr, "Sorry, video feature is empty! ");
        throwDerivedException("Video Feature Error");
	}

	return;
}

/*! @function
********************************************************************************
*******************************************************************************/ 
void Classifier::setModelPath (const std::string& _modelPath)
{
	m_modelPath = _modelPath;

 #ifdef __linux__
    if (access(m_modelPath.c_str(), 0)) {
		__printLog (std::cerr, "Sorry, the model path doesn't exist! ");
        throwDerivedException("Model Path Error");
#endif
	}

	return;
}

/*! @function
********************************************************************************
*******************************************************************************/ 
std::ostream& Classifier::__printLog (
	std::ostream& _os, const std::string& _msg) const
{
	if (std::cerr == _os) {
		_os << "Warning : " << _msg << std::endl;
	} else if (std::clog == _os) {
		_os << "Log : " << _msg << std::endl;
	} else {
		_os << "Message : " << _msg << std::endl;
	}

	return _os;
}

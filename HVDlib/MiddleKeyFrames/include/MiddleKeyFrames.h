//new header file
#ifndef _MIDDLEKEYFRAME_H_
#define _MIDDLEKEYFRAME_H_

#pragma once

#include "../../KeyFrames/include/KeyFrames.h"


namespace vd 
{

	class MiddleKeyFrames : public KeyFrames
	{

	public:

        MiddleKeyFrames (void);

		explicit MiddleKeyFrames (
			const std::string& _videoName, 
			const std::size_t  _shotInterval = 200, 
			const std::size_t _beg = 0, 
			const std::size_t _end = UINT_MAX
        );

        ~MiddleKeyFrames();

	public:

        const bool extractKeyFrames (void) override;

	private:

        static const std::size_t FIXED_HIGHT = 400;
	};

}

 #endif //_MIDDLEKEYFRAME_H_

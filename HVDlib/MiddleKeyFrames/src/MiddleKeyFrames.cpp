/*! @file
********************************************************************************
<PRE>

</PRE>
********************************************************************************


*******************************************************************************/ 


#include "../../MiddleKeyFrames/include/MiddleKeyFrames.h"

using namespace std;
using namespace cv;
using namespace vd;

/*! @function
********************************************************************************
<PRE>

</PRE>
*******************************************************************************/ 
MiddleKeyFrames::MiddleKeyFrames (void) : 
	KeyFrames()
{
	return;
}

/*! @function
********************************************************************************
<PRE>

</PRE>
*******************************************************************************/ 
MiddleKeyFrames::MiddleKeyFrames (
	const std::string& _videoName, 
	const std::size_t  _shotInterval, 
	const std::size_t _beg, 
	const std::size_t _end
	) :
	KeyFrames(_videoName, _shotInterval, _beg, _end)
{
	return;
}

/*! @function
********************************************************************************
<PRE>

</PRE>
*******************************************************************************/ 
MiddleKeyFrames::~MiddleKeyFrames (void)
{
	return;
}

/*! @function
********************************************************************************
<PRE>

</PRE>
*******************************************************************************/ 
const bool MiddleKeyFrames::extractKeyFrames (void)
{
    cv::VideoCapture iVC(m_videoName);

	const std::size_t framesNum(
        static_cast<std::size_t>(iVC.get(CV_CAP_PROP_FRAME_COUNT)));

    if (m_end > framesNum) {
		m_end = framesNum; 
	}

    const std::size_t sfNum = m_end - m_beg;

    if (framesNum < (m_shotInterval/2+1)) {
		__printLog(std::cerr, "The shot interval is too long, please reset.");
		return false;
	}



	const double scale = iVC.get(CV_CAP_PROP_FRAME_HEIGHT)/FIXED_HIGHT;
	int srcRows = static_cast<int>(
		iVC.get(CV_CAP_PROP_FRAME_WIDTH)/scale);
	int srcCols = static_cast<int>(
		iVC.get(CV_CAP_PROP_FRAME_HEIGHT)/scale);
	cv::Size srcSize(srcRows, srcCols);



	for (std::size_t i=m_beg + (m_shotInterval/2); i<(m_beg+sfNum-1); i+=m_shotInterval)
	{
        cv::Mat frameTemp;

		iVC.set(CV_CAP_PROP_POS_FRAMES, i);

		iVC >> frameTemp;

		if (frameTemp.empty())
			break;

		cv::resize(frameTemp, frameTemp, srcSize);
		m_pKeyFrames->emplace_back(frameTemp);

		m_pFramePositions->push_back(i);

		frameTemp.release();
	}

	m_isKeyFrames = true;

	return true;
}

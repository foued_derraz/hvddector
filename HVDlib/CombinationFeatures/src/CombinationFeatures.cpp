#include "../../CombinationFeatures/include/CombinationFeatures.h"
#include "../../Myexecption/include/Myexecption.h"

using namespace std;
using namespace cv;
using namespace vd;

/*! @function
********************************************************************************/
CombinationFeatures::CombinationFeatures (void) : 
	ImageFeatures()
{
	return;
}

/*! @function
*******************************************************************************/
CombinationFeatures::CombinationFeatures (const cv::Mat& _image) :
	ImageFeatures(_image)
{
	return;
}

/*! @function
*******************************************************************************/
CombinationFeatures::~CombinationFeatures (void)
{
	return;
}

/*! @function
*******************************************************************************/
const bool CombinationFeatures::extractFeatures (void)
{
	if (!m_isImage) {
		__printLog(std::cerr, "Please, load image first!");
        throwDerivedException("Image Error file");
		return false;
	}

    m_pFeatures.reset(new std::vector<double>);

    StatisticsFeatures cSTA (m_image);
    StochasticFeatures cSTO (m_image);
    EmotionFeatures cEF  (m_image);
    HarmonyFeatures cHF  (m_image);

    std::shared_ptr<std::vector<double> > staFeatures;
    std::shared_ptr<std::vector<double> > stoFeatures;
    std::shared_ptr<std::vector<double> > efFeatures;
    std::shared_ptr<std::vector<double> > hfFeatures;

	if (cSTA.extractFeatures()) {
		staFeatures = cSTA.getFeatures();
	} else {
		__printLog(std::cerr, "Failed to extract statistics features! ");
        throwDerivedException("Statistics features Error!");
		return false;
	}

	if (cSTO.extractFeatures()) {
		stoFeatures = cSTO.getFeatures();
	} else {
		__printLog(std::cerr, "Failed to extract stochastic features! ");
        throwDerivedException("Statistics features Error!");
		return false;
	}

	if (cEF.extractFeatures()) {
		efFeatures = cEF.getFeatures();
	} else {
		__printLog(std::cerr, "Failed to extract emotion features! ");
        throwDerivedException("Emotion features Error!");
		return false;
	}

	if (cHF.extractFeatures()) {
		hfFeatures = cHF.getFeatures();
	} else {
		__printLog(std::cerr, "Failed to extract harmony features! ");
        throwDerivedException("Harmony featuresError!");
		return false;
	}

/*! @function
*******************************************************************************/
	m_pFeatures->insert(m_pFeatures->end(), staFeatures->begin(), staFeatures->end());
	m_pFeatures->insert(m_pFeatures->end(), stoFeatures->begin(), stoFeatures->end());
	m_pFeatures->insert(m_pFeatures->end(), efFeatures->begin(), efFeatures->end());
	m_pFeatures->insert(m_pFeatures->end(), hfFeatures->begin(), hfFeatures->end());

    m_isFeatures = true;
/******************************************************************************/
	return true;
}

#ifndef _COMBINATIONFEATURES_H_
#define _COMBINATIONFEATURES_H_

#include "../../ImageFeatures/include/ImageFeatures.h"
#include "../../EmotionFeatures/include/EmotionFeatures.h"
#include "../../HarmonyFeatures/include/HarmonyFeatures.h"
#include "../../StatisticsFeatures/include/StatisticsFeatures.h"
#include "../../StochasticFeatures/include/StochasticFeatures.h"

#pragma once
namespace vd 
{
	class CombinationFeatures : public ImageFeatures
	{

	public:

        CombinationFeatures (void);

        explicit CombinationFeatures (const cv::Mat& _image);

        ~CombinationFeatures (void);

	public:

        const bool extractFeatures (void) override;

	};

}

#endif

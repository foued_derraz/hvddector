#include <fstream>

#include "../../svm/include/svm.h"

#include "../../Classifier/include/Classifier.h"
#pragma once

namespace vd
{
	/*! @class
	********************************************************************************
	<PRE>

	</PRE>
	*******************************************************************************/
	class SvmClassifier : Classifier 
	{
	public:

        SvmClassifier(
            const cv::Mat& _videoFeature,
            const std::string& _modelPath
		);

        ~SvmClassifier(void);

	public:

        const double calculateResult (void)  override;

	private:

        static const std::string NORM_NAME;

        static const std::string SVM_MODEL_NAME;

        static bool m_mutex;

	private:

        svm_node* m_node;

        svm_model* m_model;

	private:

        const double _predictValue (void) const override;

        void _initModel (void) override;

	private:

        void __transSvmNode (const std::string& _normName);

	};
}

#ifndef _STATISTICSFEATURES_H_
#define _STATISTICSFEATURES_H_

#pragma once

#include "../../ImageFeatures/include/ImageFeatures.h"


namespace vd 
{

	/*! @class
	********************************************************************************
	<PRE>

	</PRE>
	*******************************************************************************/
	class StatisticsFeatures : public ImageFeatures
	{

	public:

        StatisticsFeatures (void);

        explicit StatisticsFeatures (const cv::Mat& _image);

        ~StatisticsFeatures (void);

	public:

        const bool extractFeatures (void) override;

	private:

        static const std::size_t FEATURE_NUM = 9;

	private:

		void __calculateHSV (
            std::vector<double>& _hsvFeatures,
            double& _lightnessKey
            );

		void __calculateHLS (
            double& _lightnessMedian,
            double& _shadowProportion
            );
	};

}


#endif //

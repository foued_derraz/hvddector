#ifndef _EMOTIONFEATURES_H_
#define _EMOTIONFEATURES_H_

#include "../../ImageFeatures/include/ImageFeatures.h"

namespace vd 
{

	/*! @class
	********************************************************************************

	*******************************************************************************/
	class EmotionFeatures : public ImageFeatures
	{

	public:

        EmotionFeatures (void);

        explicit EmotionFeatures (const cv::Mat& _image);

        ~EmotionFeatures (void);

	public:

        const bool extractFeatures (void) override;

	private:

        static const std::size_t FEATURE_NUM = 64;

        static const double MY_PI;

		

        static const double MAX_ACTIVITY;
        static const double MIN_ACTIVITY;
        static const double MAX_WEIGHT;
        static const double MIN_WEIGHT;
        static const double MAX_HEAT;
        static const double MIN_HEAT;

	private:

		inline void __processPixel (
            const cv::Scalar& _s, double& _activity, double& _weight, double& _heat) const;

		inline const std::size_t __calculateIndex (
            const double _activity, const double _weight, const double _heat) const;

	};

}
#endif

#include "../../EmotionFeatures/include/EmotionFeatures.h"
#include "../../Myexecption/include/Myexecption.h"


using namespace std;
using namespace cv;
using namespace vd;


/*

*/

const double EmotionFeatures::MY_PI = 3.1415926536;
const double EmotionFeatures::MAX_ACTIVITY = 16.1235*0.5;
const double EmotionFeatures::MIN_ACTIVITY = 6.77574*1.5;
const double EmotionFeatures::MAX_WEIGHT = 2.45524*0.5;
const double EmotionFeatures::MIN_WEIGHT = -7.74189*0.5;
const double EmotionFeatures::MAX_HEAT = 5.41763*0.5;
const double EmotionFeatures::MIN_HEAT = 0.426733*1.5;

/********************************************************************************/ 
EmotionFeatures::EmotionFeatures (void) : 
	ImageFeatures()
{
	return;
}

/*******************************************************************************/ 
EmotionFeatures::EmotionFeatures (const cv::Mat& _image) :
	ImageFeatures(_image)
{
	return;
}

/********************************************************************************/ 
EmotionFeatures::~EmotionFeatures (void)
{
	return;
}

/********************************************************************************/ 
const bool EmotionFeatures::extractFeatures (void)
{
    if (!m_isImage) {
		__printLog(std::cerr, "Please load image first!");
        //throwDerivedException("Image Error file");
		return false;
	}

    m_pFeatures.reset(new std::vector<double>(FEATURE_NUM));

    cv::Scalar s;

    double activity(0.0);
    double weight(0.0);
    double heat(0.0);

    cv::Mat labImage;
	cv::cvtColor(m_image, labImage, CV_BGR2Lab);

	for (int i=0; i<labImage.rows; ++i)
	{
		for (int j=0; j<labImage.cols; ++j)
		{
			s.val[0] = labImage.at<cv::Vec3b>(i,j)[0];
			s.val[1] = labImage.at<cv::Vec3b>(i,j)[1];
			s.val[2] = labImage.at<cv::Vec3b>(i,j)[2];

            __processPixel(s, activity, weight, heat);

			const std::size_t indexEmotion = 
                __calculateIndex(activity, weight, heat);

			++(*m_pFeatures)[indexEmotion];
		}
	}

	const double product = labImage.rows*labImage.cols;
	for (int i=0; i<FEATURE_NUM ; ++i) {
		(*m_pFeatures)[i] /= product;
	}

	labImage.release();

    m_isFeatures = true;

	return true;
}

/********************************************************************************/ 
void EmotionFeatures::__processPixel (
	const cv::Scalar& _s, double& _activity, double& _weight, double& _heat) const
{
    const double chroma = sqrt(pow(_s.val[1],2)+pow(_s.val[2],2));
    const double hue = atan(static_cast<double>(_s.val[2])/static_cast<double>(_s.val[1]));

	_activity = -2.1+0.06*sqrt(pow(_s.val[0]-50,2)+pow(_s.val[1]-3,2)+pow(_s.val[2]-17,2));
	_weight = -1.8+0.04*(100-_s.val[0])+0.45*cos(hue-100*MY_PI/180);
	_heat = -0.5+0.02*pow(chroma,1.07)*cos(hue-100*MY_PI/180);

	return;
}

/********************************************************************************/ 
const std::size_t EmotionFeatures::__calculateIndex (
	const double _activity, const double _weight, const double _heat) const
{
	const double normActivity = (_activity-MIN_ACTIVITY)/(MAX_ACTIVITY-MIN_ACTIVITY);
	const double normWeight = (_weight-MIN_WEIGHT)/(MAX_WEIGHT-MIN_WEIGHT);
	const double normHeat = (_heat-MIN_HEAT)/(MAX_HEAT-MIN_HEAT);

	double indexActivity = normActivity*48.0;
	if ( indexActivity < 0 ) {
		indexActivity = 0.0;
	} else if ( indexActivity > 48.0 ) {
		indexActivity = 48.0;
	}

	double indexWeight = normWeight*12.0;
	if ( indexWeight < 0 ) {
		indexWeight = 0.0;
	} else if ( indexWeight > 12.0 ){
		indexWeight = 12.0;
	}

	double indexHeat = normHeat*3.0;
	if ( indexHeat < 0 ) {
		indexHeat = 0.0;
	} else if ( indexHeat >3.0) {
		indexHeat = 3.0;
	}

	const std::size_t index = static_cast<std::size_t>(indexHeat + indexWeight + indexActivity);

	return index;
}

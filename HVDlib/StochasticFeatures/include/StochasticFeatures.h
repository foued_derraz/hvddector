#include "../../ImageFeatures/include/ImageFeatures.h"
#pragma once

namespace vd 
{

	/*! @class
	********************************************************************************
	<PRE>

	</PRE>
	*******************************************************************************/
	class StochasticFeatures : public ImageFeatures
	{

	public:

        StochasticFeatures (void);

        explicit StochasticFeatures (const cv::Mat& _image);

        ~StochasticFeatures (void);
	public:

        const bool extractFeatures (void) override;

	private:

        static const int MY_SIGMA;
        static const double MY_PI;
        static const double MY_E;

        static const std::size_t FEATURE_NUM = 2;

	private:

		void __weibullMle (
            double& _scale,
            double& _shape
            );

		const double ___newton (
			const double _g, 
			const std::vector<double>& _x
            );

        void ___gaussianFilter (cv::Mat& _gauss_mat);
	};

}

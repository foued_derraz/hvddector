#ifndef _VIDEODETECTOR_H_
#define _VIDEODETECTOR_H_

#include <iostream>
#include <string>
#include <vector>

#include <opencv2/opencv.hpp>

#include "../../SceneIntegration/include/SceneIntegration.h"
#include "../../MiddleKeyFrames/include/MiddleKeyFrames.h"
#include "../../SvmClassifier/include/SvmClassifier.h"

#pragma once

namespace vd 
{

	class VideoDetector
	{
	public:
		explicit VideoDetector (
			const std::string& _fileName,
			const std::string& _modelPath = ".",
			const std::size_t _shotInterval = 100,
			const std::size_t _sceneInterval = 2000
        );

        ~VideoDetector(void);

	public:

        const std::size_t getSceneNum (void);

        const bool detectVideo (void);

        const std::vector<double> getResultSet (void) const;

		const std::vector<std::string> storeKeyFrames (
			const std::string& _folderPath, 
			const std::string& _imageName
        );

        static void help (void);
	private:

        std::string m_fileName;

        std::string m_modelPath;

        std::size_t m_shotInterval;

        std::size_t m_sceneInterval;

        std::vector<double> m_resultSet;
	private:

		inline static std::ostream& _printLog (
            std::ostream& _os, const std::string& _msg);
	};
}

#endif /* _VIDEODETECTOR_H_*/

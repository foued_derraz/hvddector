#include "../../VideoDetector/include/VideoDetector.h"
#include "../../Myexecption/include/Myexecption.h"
#include <stdexcept>
#include <iostream>

#ifdef __linux__
#include <unistd.h>
#endif

using namespace std;
using namespace cv;
using namespace vd;

/********************************************************************************/
VideoDetector::VideoDetector (
	const std::string& _fileName,
	const std::string& _modelPath,
	const std::size_t _shotInterval,
	const std::size_t _sceneInterval
) : 
	m_fileName(_fileName),
	m_modelPath(_modelPath),
	m_shotInterval(_shotInterval),
	m_sceneInterval(_sceneInterval)
{
	std::size_t maxShotInterval(999); 
	std::size_t maxSceneInterval(9999);

	

	cv::VideoCapture iVC (m_fileName);
	if (!iVC.isOpened()) {
		_printLog(std::cerr, "Failed to open the video!");
        throwDerivedException("Video Name Error");
	}

	

	if (m_shotInterval < 1 || m_shotInterval > maxShotInterval) {
		_printLog(std::cerr, "The value of the shot interval is invalid.");
        throwDerivedException("Shot Interval Error");
	}

	
	if (m_sceneInterval < 1 || m_sceneInterval > maxSceneInterval) {
		_printLog(std::cerr, "The value of the scene interval is invalid.");
        throwDerivedException("Scene Interval Error");
	}


#ifdef __linux__
    if (access(m_modelPath.c_str(), 0)) {
		_printLog (std::cerr, "Sorry, the model path doesn't exist! ");
        throwDerivedException("Model Path Error");
	}

#endif
	return;
}

/********************************************************************************/
VideoDetector::~VideoDetector(void)
{
	return;
}

/********************************************************************************/
const bool VideoDetector::detectVideo (void)
{


	cv::VideoCapture iVC (m_fileName);

	const std::size_t framesNum(
        static_cast<std::size_t>(iVC.get(CV_CAP_PROP_FRAME_COUNT)));
	SceneIntegration iSI(m_fileName, m_shotInterval, m_sceneInterval);

    double finalLabel (0.0);
	std::size_t beg (0);
	std::size_t sceneNum(0);

	for (std::size_t j=0; j<framesNum; j+=m_sceneInterval)
	{

		std::shared_ptr<cv::Mat> pFeatures;
		pFeatures = iSI.extractSceneFeatures(beg);

		if (pFeatures == nullptr) {
			_printLog(std::cerr, "Failed to extract the feature!");
			return false;
		}


		SvmClassifier iSC((*pFeatures), m_modelPath);
		finalLabel = iSC.calculateResult();

		std::cout << " Scene Prop = " << finalLabel << std::endl;


		m_resultSet.push_back(finalLabel);

		beg += m_sceneInterval;

		++sceneNum;

		pFeatures.reset(new cv::Mat);
	}

	return true;
}

/********************************************************************************/
const std::vector<double> VideoDetector::getResultSet(void) const
{
	if (m_resultSet.empty())
    throwDerivedException("Result Set Empty");
	return m_resultSet;
}

/********************************************************************************/
const std::size_t VideoDetector::getSceneNum (void)
{
    std::size_t sceneNum (0);

	cv::VideoCapture iVC (m_fileName);

	const std::size_t framesNum(
        static_cast<std::size_t>(iVC.get(CV_CAP_PROP_FRAME_COUNT)));

	if (framesNum < m_sceneInterval) {
		sceneNum = 1;
	} else {
		for (std::size_t j=0; j<framesNum; j+=m_sceneInterval) {
			++sceneNum;
		}
	}

	return sceneNum;
}

/********************************************************************************/
const std::vector<std::string> VideoDetector::storeKeyFrames (
	const std::string& _folderPath, const std::string& _imageName)
{
    std::vector<std::string> names;
	cv::VideoCapture cVC (m_fileName);

	const std::size_t framesNum(
        static_cast<std::size_t>(cVC.get(CV_CAP_PROP_FRAME_COUNT)));

	std::size_t beg (0);
	std::size_t sceneNum(0);
	std::vector<cv::Mat> keyFrames;
	std::vector<std::size_t> framePositions;

	for (std::size_t i = 0; i<framesNum-1; i+=m_sceneInterval)
	{
		MiddleKeyFrames iMKF(m_fileName, m_shotInterval, beg, beg+m_sceneInterval);

		if(!iMKF.extractKeyFrames() ) {
			_printLog(std::cerr, "Failed to extract key frames.");
			return names;
		}

		keyFrames = *iMKF.getKeyFrames();
		framePositions = *iMKF.getFramePositions();

		std::string name;

		std::string ul("_");
		std::stringstream ss;
		ss << framePositions[framePositions.size()/2];
		std::string digit = ss.str();
		std::string jpg(".jpg");

		name.append(_folderPath);
		name.append("/");
		name.append(_imageName);
		name.append(ul);
		name.append(digit);
		name.append(jpg);

		if(keyFrames[keyFrames.size()/2].data) {
			cv::imwrite(name, keyFrames[keyFrames.size()/2]);
			names.push_back(name);
		} 

		beg += m_sceneInterval;

		++sceneNum;
	}
	
	return names;
}

/********************************************************************************/
void VideoDetector::help (void)
{
    const std::string version = "Version 1.0";
    std::cout << std::endl;
    std::cout << "version new" << version << ")" << std::endl;
    std::cout << std::endl << std::endl;

    return;
}

/********************************************************************************/
std::ostream& VideoDetector::_printLog (
	std::ostream& _os, const std::string& _msg)
{
	if (std::cerr == _os) {
		_os << "Warning : " << _msg << std::endl;
	} else if (std::clog == _os) {
		_os << "Log : " << _msg << std::endl;
	} else {
		_os << "Message : " << _msg << std::endl;
	}

	return _os;
}

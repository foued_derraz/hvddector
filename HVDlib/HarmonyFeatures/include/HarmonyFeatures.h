#ifndef _HARMONYFEATURES_H_
#define _HARMONYFEATURES_H_


#include "../../ImageFeatures/include/ImageFeatures.h"

namespace vd 
{

/*! @class
*******************************************************************************/
	class HarmonyFeatures : public ImageFeatures
	{

	public:

		HarmonyFeatures (void); 

		explicit HarmonyFeatures (const cv::Mat& _image); 

        ~HarmonyFeatures (void);

	public:

		const bool extractFeatures (void) override; //

	private:

		static const std::size_t FEATURE_NUM = 26; //

        static const double MY_PI;
        static const double MY_E;

        static const double MAX_HARMONY;
        static const double MIN_HARMONY;

	private:

		inline const double __calculateHarmonyScore (
			const cv::Scalar& _s1, const double _L2, const double _A, const double _B); //

		inline const std::size_t __calculateIndex (const double _harmonyScore); //

	};

}
#endif /* _HARMONYFEATURES_H_ */

#ifndef _KEYFRAMES_H_
#define _KEYFRAMES_H_
#pragma once

#include <iostream>
#include <fstream>
#include <memory>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>

#ifdef __linux__
#include <unistd.h>
#endif

#include <cstddef>
#include <climits>

#include <opencv2/opencv.hpp>

//
namespace vd 
{
/*! @class
*******************************************************************************/

	class KeyFrames 
	{
	public:

		KeyFrames (void); 

		explicit KeyFrames (
			const std::string& _videoName, 
			const std::size_t  _shotInterval = 200, 
			const std::size_t _beg = 0, 
			const std::size_t _end = UINT_MAX
        );

        virtual ~KeyFrames();

	public:

		void loadVideo (
			const std::string& _videoName, 
			const std::size_t  _shotInterval = 200, 
			const std::size_t _beg = 0, 
			const std::size_t _end = UINT_MAX
            );

        virtual const bool extractKeyFrames (void) = 0;
        void storeKeyFrames (const std::string& _folderPath, const std::string& _imageName) const;
        const std::shared_ptr<std::vector<cv::Mat> > getKeyFrames (void) const;
        const std::shared_ptr<std::vector<std::size_t> > getFramePositions (void) const;

	protected:

        std::string m_videoName;
        std::size_t  m_shotInterval;
        std::size_t m_beg;
        std::size_t m_end;
        bool m_isVideo;
        bool m_isKeyFrames;
        std::shared_ptr<std::vector<cv::Mat> > m_pKeyFrames;
        std::shared_ptr<std::vector<std::size_t> > m_pFramePositions;

	protected:

        inline void __resetData (void);

        inline std::ostream& __printLog (std::ostream& os, const std::string& msg) const;

	};
}


#endif //

#include "../../KeyFrames/include/KeyFrames.h"

using namespace std;
using namespace cv;
using namespace vd;

/*! @function
*******************************************************************************/
KeyFrames::KeyFrames (void) : 
    m_isVideo(false),
    m_isKeyFrames(false),
    m_pKeyFrames(new std::vector<cv::Mat>),
    m_pFramePositions(new std::vector<std::size_t>)
{
	return;
}

/*! @function
*******************************************************************************/
KeyFrames::KeyFrames (
	const std::string& _videoName, 
	const std::size_t  _shotInterval, 
	const std::size_t _beg, 
	const std::size_t _end
) :
    m_videoName(_videoName),
    m_shotInterval(_shotInterval),
    m_beg(_beg),
    m_end(_end),
    m_isKeyFrames(false),
    m_pKeyFrames(new std::vector<cv::Mat>),
    m_pFramePositions(new std::vector<std::size_t>)
{
	cv::VideoCapture iVC(m_videoName);

    if (!iVC.isOpened()) {
		__printLog(std::cerr, "Failed to open the video in <KeyFrames>!");
//		throw std::exception("Video Error");
	}

    m_isVideo = true;

	return;
}

/*! @function
*******************************************************************************/
KeyFrames::~KeyFrames (void)
{
	return;
}

/*! @function
*******************************************************************************/
void KeyFrames::loadVideo (
	const std::string& _videoName, 
	const std::size_t  _shotInterval, 
	const std::size_t _beg, 
	const std::size_t _end
)
{
    __resetData();

	m_videoName = _videoName;
    m_shotInterval = _shotInterval;
    m_beg = _beg;
    m_end = _end;

	cv::VideoCapture iVC(m_videoName);

    if (!iVC.isOpened()) {
		__printLog(std::cerr, "Failed to open the video in <KeyFrames>!");
	}


	m_isVideo = true;

	return;
}

/*! @function
*******************************************************************************/
void KeyFrames::storeKeyFrames (
	const std::string& _folderPath, 
	const std::string& _imageName) const
{
#ifdef __linux__

    if (access(_folderPath.c_str(), 0)) {
		__printLog(std::cerr, "Failed to open the folder! ");
		return;
	}

#endif

    if (!m_isKeyFrames) {
		__printLog(std::cerr, "Please extract key frames first! ");
		return;
	}

    std::size_t kfNum = m_pKeyFrames->size();

	if (0 == kfNum) {
		__printLog(std::clog, "No key frames! ");
		return;
	}

	for (std::size_t i=0; i<kfNum; ++i) 
	{
        std::string name;

		std::string ul("_");
		std::stringstream ss;
		ss << i;
		std::string digit = ss.str();
		std::string jpg(".jpg");

		name.append(_folderPath);
		name.append("/");
		name.append(_imageName);
		name.append(ul);
		name.append(digit);
		name.append(jpg);

		if((*m_pKeyFrames)[i].data) {
			cv::imwrite(name, (*m_pKeyFrames)[i]);
		}
	}

	return;
}

/*! @function
*******************************************************************************/
const std::shared_ptr<std::vector<cv::Mat> > 
	KeyFrames::getKeyFrames(void) const
{
	if (m_isKeyFrames) {
		return m_pKeyFrames;
	} else {
		__printLog(std::cerr, "Please extract key frames first!");
		return nullptr;
	}
}

/*! @function
********************************************************************************
<PRE>

</PRE>
*******************************************************************************/ 
const std::shared_ptr<std::vector<std::size_t> > 
	KeyFrames::getFramePositions(void) const
{
	if (m_isKeyFrames) {
		return m_pFramePositions;
	} else {
		__printLog(std::cerr, "Please extract key frames first!");
		return nullptr;
	}
}

/*! @function
********************************************************************************
<PRE>

</PRE>
*******************************************************************************/ 
void KeyFrames::__resetData (void)
{
	m_videoName.clear();

	m_isVideo = false;

	m_isKeyFrames = false;

	if (m_pKeyFrames != nullptr) {
		m_pKeyFrames->clear();
		m_pKeyFrames.reset(new std::vector<cv::Mat>);
	}

	if (m_pFramePositions != nullptr) {
		m_pFramePositions->clear();
		m_pFramePositions.reset(new std::vector<std::size_t>);
	}

	return;
}

/********************************************************************************/
std::ostream& KeyFrames::__printLog (
	std::ostream& _os, const std::string& _msg) const
{
	if (std::cerr == _os) {
		_os << "Warning : " << _msg << std::endl;
	} else if (std::clog == _os) {
		_os << "Log : " << _msg << std::endl;
	} else {
		_os << "Message : " << _msg << std::endl;
	}

	return _os;
}

#ifndef _IMAGEFEATURES_H_
#define _IMAGEFEATURES_H_
#pragma once

#include <iostream>
#include <memory>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>

#include <opencv2/opencv.hpp>


//
namespace vd 
{
/*! @class
*******************************************************************************/
	class ImageFeatures
	{

	public:

        ImageFeatures (void);
        explicit ImageFeatures (const cv::Mat& _image);
        virtual ~ImageFeatures (void);

	public:

        void loadImage (const cv::Mat& _image);
        void showImage (void) const;
        virtual const bool extractFeatures (void) = 0;
		const std::shared_ptr<std::vector<double> > 
            getFeatures (void) const;

	protected:

        cv::Mat m_image;
        bool m_isImage;
        bool m_isFeatures;
        std::shared_ptr<std::vector<double> > m_pFeatures;

	protected:

        inline void __resetData (void);
        inline std::ostream & __printLog (std::ostream& _os, const std::string& _msg) const;

	};
}
#endif // _IMAGEFEATURES_H_

#include "../../ImageFeatures/include/ImageFeatures.h"
#include "../../Myexecption/include/Myexecption.h"

using namespace std;
using namespace cv;
using namespace vd;

/*! @function
********************************************************************************/
ImageFeatures::ImageFeatures (void) : m_isImage(false), m_isFeatures(false),
    m_pFeatures(new std::vector<double>)
{
	return;
}

/*! @function
*******************************************************************************/
ImageFeatures::ImageFeatures (const cv::Mat& _image) :
    m_image(_image), m_isFeatures(false), m_pFeatures(new std::vector<double>)
{
    if (!m_image.data) {
		__printLog(std::cerr, "Failed to set an image in ImageFeatures!");
        throwDerivedException("Image Error! ");

                      }

    m_isImage = true; //

	return;
}

/*! @function
*******************************************************************************/
ImageFeatures::~ImageFeatures (void)
{
	return;
}

/*! @function
*******************************************************************************/
void ImageFeatures::loadImage (const cv::Mat& _image)
{
    __resetData();

    m_image = _image;

    if (!m_image.data) {
		__printLog(std::cerr, "Failed to set an image in ImageFeatures!");        
       throwDerivedException("Image Error! ");
	}

    m_isImage = true; //

	return;
}

/*! @function
*******************************************************************************/
void ImageFeatures::showImage (void) const
{
    if (!m_isImage) { //
		__printLog(std::cerr, "Please load an image first!");
		return;
	}

    cv::imshow("Image", m_image); //
	cv::waitKey(0);

	return;
}

/*! @function
*******************************************************************************/
const std::shared_ptr<std::vector<double> > 
	ImageFeatures::getFeatures (void) const
{
	if (m_isFeatures) {
		return m_pFeatures;
	} else {
		__printLog(std::cerr, "Failed to get features!");
		return nullptr;
	}
}

/*! @function
*******************************************************************************/
void ImageFeatures::__resetData (void)
{
	if (!m_image.empty()) {
		m_image.release();
	}

	m_isImage = false;

	m_isFeatures = false;

	if (m_pFeatures != nullptr) {
		m_pFeatures->clear();
		m_pFeatures.reset(new std::vector<double>);
	}

	return;
}

/*! @function
*******************************************************************************/
std::ostream& ImageFeatures::__printLog (
	std::ostream& _os, const std::string& _msg) const
{
	if (std::cerr == _os) {
		_os << "Warning : " << _msg << std::endl;
	} else if (std::clog == _os) {
		_os << "Log : " << _msg << std::endl;
	} else {
		_os << "Message : " << _msg << std::endl;
	}

	return _os;
}
/******************************************************************************/

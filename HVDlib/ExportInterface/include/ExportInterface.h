#ifndef EXPORTINTERFACE_H_
#define EXPORTINTERFACE_H_

#include <iostream>
#include <string>
#include <vector>

#include <stdexcept>
#include <fstream>


struct SceneInfo {
	char imagePath[1024];
	double prop; 
    unsigned int bpos;
    unsigned int epos;
    double btime;
    double etime;
};

namespace vd
{

    class HVD
    {

    public:

    const unsigned int HVDSceneNum (
            const char* const _fileName,
            const unsigned int _shotInterval = 100,
            const unsigned int _sceneShotNum = 20);

/*********************************************************************************/ 
     const bool HVDDetector (
            double* _resultSet,
            const char* const _fileName,
            const char* const _modelPath,
            const unsigned int _shotInterval = 100,
            const unsigned int _sceneShotNum = 20);

/*********************************************************************************/ 
     const bool HVDDetector_Info (
            SceneInfo* _resultSet,
            const char* const _folderPath,
            const char* const _fileName,
            const char* const _modelPath,
            const unsigned int _shotInterval = 100,
            const unsigned int _sceneShotNum = 20);

/*********************************************************************************/ 
      void HVDDetector_Help(void);

      virtual ~HVD (void);



    };
}

#endif /* EXPORTINTERFACE_H_ */

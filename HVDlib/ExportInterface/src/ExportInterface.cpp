#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <memory>
#include <ctime>
#include <cstring>

#include "../../ExportInterface/include/ExportInterface.h"
#include "../../VideoDetector/include/VideoDetector.h"
#include "../../Myexecption/include/Myexecption.h"


using namespace cv;
using namespace std;
using namespace vd;

std::ostream&  _printLog (std::ostream& _os, const std::string& _msg);

/*******************************************************************************/
const unsigned int HVD::HVDSceneNum (const char* const _fileName,
    const unsigned int _shotInterval, const unsigned int _sceneShotNum)
{
	std::clog << std::endl;
    _printLog(std::clog, "----------Begin VideoScene Num! ----------");
	std::clog << std::endl;

    int sceneNum (0);

    const std::string fileName (_fileName);

	std::cout << " File Name: " << fileName << std::endl;

    cv::VideoCapture iVC (fileName);
	if(!iVC.isOpened()) {
		_printLog(std::cerr, "Failed to open the video! ");
        throwDerivedException("Video Error file");
		return 0;
	}

    std::size_t scene_length(_shotInterval*_sceneShotNum);

	const std::size_t framesNum(
        static_cast<std::size_t>(iVC.get(CV_CAP_PROP_FRAME_COUNT)));

	
	if (framesNum < scene_length) {
		sceneNum = 1;
	} else {
		for (std::size_t j=0; j<framesNum-1; j+=scene_length){
			++sceneNum;
		}
	}

	std::clog << " Frames Number = " << framesNum << std::endl;
	std::clog << " Scenes Number = " << sceneNum << std::endl;

	std::clog << std::endl;
    _printLog(std::clog, "----------End HVDVideoSceneNum! ----------");
	std::clog << std::endl;

	return sceneNum;
}

/*
*******************************************************************************/
const bool HVD::HVDDetector (double* _resultSet,	const char* const _fileName,
	const char* const _modelPath,
	const unsigned int _shotInterval, 
	const unsigned int _sceneShotNum
)
{
	std::clog << std::endl;
    _printLog(std::clog, "----------Begin HVDDetector! ----------");
	std::clog << std::endl;

	const std::string fileName(_fileName);
	const std::string modelPath(_modelPath);

	const std::size_t shotInterval(_shotInterval);
	const std::size_t sceneInterval(_sceneShotNum*shotInterval);

	std::cout << " File Name: " << fileName << std::endl;

	cv::VideoCapture iVC (fileName); //
	if(!iVC.isOpened()) {
		_printLog(std::cerr, "Failed to open the video! ");
		return false;
	}

	const double framesNum (iVC.get(CV_CAP_PROP_FRAME_COUNT)); //
    const double fps (iVC.get(CV_CAP_PROP_FPS));
	double vtime = framesNum / fps;
	std::cout << " Frames Number: " << framesNum << std::endl;
	std::cout << " Video Time: " << vtime << "s " << std::endl;


	std::cout << " Please wait some time! :)" << std::endl;

	std::vector<double> reseltSet;

	VideoDetector iVD(fileName, modelPath, shotInterval, sceneInterval);

	clock_t start=clock();

	if (iVD.detectVideo()) {
		reseltSet = iVD.getResultSet();
	} else {
		return false;
	}

	clock_t end=clock();

	double ptime = static_cast<double>(end-start)/CLOCKS_PER_SEC; //

	std::cout << " Processing Time: " << ptime << "s " << std::endl;

	for (std::size_t i=0; i<reseltSet.size(); ++i) {
		_resultSet[i] = reseltSet[i];
		std::cout << " Scene[" << i << "] Result : " << _resultSet[i] << std::endl;
	}

	std::clog << std::endl;
	_printLog(std::clog, "----------End HorrorVideoDetector! ----------");
	std::clog << std::endl;

	return true;
}

/*
*******************************************************************************/
const bool HVD::HVDDetector_Info (
	SceneInfo* _infoSet,
	const char* const _folderPath,
	const char* const _fileName, 
	const char* const _modelPath,
	const unsigned int _shotInterval, 
	const unsigned int _sceneShotNum
)
{
	std::clog << std::endl;
    _printLog(std::clog, "----------Begin HVDDetector_Image! ----------");
	std::clog << std::endl;

	const std::string fileName(_fileName);
	const std::string modelPath(_modelPath);

	const std::string folderPath(_folderPath);
	const std::string imageName(_fileName);

	const std::size_t shotInterval(_shotInterval);
	const std::size_t sceneInterval(_sceneShotNum*shotInterval);

	std::cout << " File Name: " << fileName << std::endl;

	cv::VideoCapture iVC (fileName); //
	if(!iVC.isOpened()) {
		_printLog(std::cerr, "Failed to open the video! ");
		return false;
	}

	const double framesNum (iVC.get(CV_CAP_PROP_FRAME_COUNT)); //
	const double fps (iVC.get(CV_CAP_PROP_FPS)); //
	double vtime = framesNum / fps;
	std::cout << " Frames Number: " << framesNum << std::endl;
	std::cout << " Video Time: " << vtime << "s " << std::endl;


	std::cout << " Please wait some time! :)" << std::endl;

	std::vector<double> reseltSet;
	std::vector<std::string> nameSet;

	VideoDetector iVD(fileName, modelPath, shotInterval, sceneInterval);

	clock_t start=clock();

	if (iVD.detectVideo()) {
		reseltSet = iVD.getResultSet();
		nameSet = iVD.storeKeyFrames (folderPath, imageName); //
	} else {
		return false;
	}

	clock_t end=clock();

	double ptime = static_cast<double>(end-start)/CLOCKS_PER_SEC; //

	std::cout << " Processing Time: " << ptime << "s " << std::endl;

	for (std::size_t i=0; i<reseltSet.size(); ++i) {
		_infoSet[i].prop = reseltSet[i];
        //strcpy_s(_infoSet[i].imagePath, nameSet[i].c_str());
        strcpy(_infoSet[i].imagePath, nameSet[i].c_str());
		//std::cout << " Scene[" << i << "] Result : " << _infoSet[i].prop << std::endl;
		//std::cout << " Scene[" << i << "] Image : " << _infoSet[i].imagePath << std::endl;
	}

	unsigned int sceneNum (0); //
	if (framesNum < sceneInterval) {
		_infoSet[0].bpos = 0;
		_infoSet[0].epos = static_cast<size_t>(framesNum);
		_infoSet[0].btime = static_cast<double>(_infoSet[0].bpos)/fps;
		_infoSet[0].etime = static_cast<double>(_infoSet[0].epos)/fps;
	} else {
		for (std::size_t j=0; j<framesNum-1; j+=sceneInterval){
			_infoSet[sceneNum].bpos = j;
			if (j < framesNum-1-sceneInterval) {
				_infoSet[sceneNum].epos = j+sceneInterval;
			} else {
				_infoSet[sceneNum].epos =static_cast<std::size_t>(framesNum-1);
			}
			_infoSet[sceneNum].btime = static_cast<double>(_infoSet[sceneNum].bpos)/fps;
			_infoSet[sceneNum].etime = static_cast<double>(_infoSet[sceneNum].epos)/fps;
			++sceneNum;
		}
	}

	std::clog << std::endl;
    _printLog(std::clog, "----------End HVDDetector_Image! ----------");
	std::clog << std::endl;

	return true;
}


/**********************************************************************************/
void HVD::HVDDetector_Help (void)
{
	const std::string version = "Version 1.0";
	std::cout << std::endl;
	std::cout << "   " << std::endl;
	std::cout << std::endl << std::endl;

	std::cout << std::endl << std::endl;

	return;
}

/**********************************************************************************/


HVD::~HVD (void)
{
    return;
}
/********************************************************************************/ 
std::ostream& _printLog (
	std::ostream& _os, const std::string& _msg)
{
	if (std::cerr == _os) {
		_os << "Warning : " << _msg << std::endl;
	} else if (std::clog == _os) {
		_os << "Log : " << _msg << std::endl;
	} else {
		_os << "Message : " << _msg << std::endl;
	}

	return _os;
}

/*********************************************************************************/

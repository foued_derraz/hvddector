#ifndef _SCENEINTEGRATION_H_
#define _SCENEINTEGRATION_H_


#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <ctime>

#include <opencv2/opencv.hpp>

#include "../../CombinationFeatures/include/CombinationFeatures.h"
#include "../../MiddleKeyFrames/include/MiddleKeyFrames.h"


namespace vd
{
	/*! @class
	********************************************************************************
	<PRE>

	</PRE>
	*******************************************************************************/
	class SceneIntegration
	{
	public:

		explicit SceneIntegration (
            const std::string& _fileName,
            const std::size_t _shotInterval = 100,
            const std::size_t _sceneInterval = 2000
		);

		~SceneIntegration (void);

		std::shared_ptr<cv::Mat> extractSceneFeatures (
            const std::size_t _beg = 0);

		void getKeyInformation (
			std::vector<cv::Mat>& _keyFrames, 
			std::vector<std::size_t>& _framePositions
        );

	private:

        std::string m_fileName;
        std::size_t m_shotInterval;
        std::size_t m_sceneInterval;

        std::shared_ptr<std::vector<cv::Mat> > m_pKeyFrames;
        std::shared_ptr<std::vector<std::size_t> > m_framePositions;

	private:

		std::shared_ptr<std::vector<double> > 
            _detectImage (const cv::Mat& _keyFrame);

		inline static std::ostream& _printLog (
            std::ostream& _os, const std::string& _msg);
	};
}

#endif //

#include "../../SceneIntegration/include/SceneIntegration.h"

using namespace std;
using namespace cv;
using namespace vd;

/*! @function
********************************************************************************
<PRE>

</PRE>
*******************************************************************************/ 
SceneIntegration::SceneIntegration (
	const std::string& _fileName, 
	const std::size_t _shotInterval,
	const std::size_t _sceneInterval
	) : 
	m_fileName(_fileName),
	m_shotInterval(_shotInterval),
	m_sceneInterval(_sceneInterval),
	m_pKeyFrames(new std::vector<cv::Mat>),
	m_framePositions(new std::vector<std::size_t>)
{
	std::size_t maxShotInterval(999); 
	std::size_t maxSceneInterval(9999);


	std::ifstream ifile;
	ifile.open(m_fileName, ios::in);
	if (ifile.fail()) {
		_printLog(std::cerr, "Failed to open the video!");
//		throw std::exception("File Name Error");
	}
	ifile.close();


	if (m_shotInterval < 1 || m_shotInterval > maxShotInterval) {
		_printLog(std::cerr, "The value of the shot interval is invalid.");
//		throw std::exception("Shot Interval Error");
	}


	if (m_sceneInterval < 1 || m_sceneInterval > maxSceneInterval) {
		_printLog(std::cerr, "The value of the scene interval is invalid.");
//		throw std::exception("Scene Interval Error");
	}

	return;
}

/*! @function
********************************************************************************
<PRE>

</PRE>
*******************************************************************************/ 
SceneIntegration::~SceneIntegration (void)
{
	return;
}

/*! @function
********************************************************************************
<PRE>

</PRE>
*******************************************************************************/ 
std::shared_ptr<cv::Mat> SceneIntegration::extractSceneFeatures (const std::size_t _beg) 
{
    cv::VideoCapture cVC (m_fileName);

	if (!cVC.isOpened()) {
		_printLog(std::cerr, "Failed to open the video!");
		return nullptr;
	}

	std::size_t framesNum = 
        static_cast<std::size_t>(cVC.get(CV_CAP_PROP_FRAME_COUNT));

	MiddleKeyFrames iMKF(m_fileName, m_shotInterval, _beg, _beg+m_sceneInterval);

	if(!iMKF.extractKeyFrames() ) {
		_printLog(std::cerr, "Failed to extract key frames.");
		return nullptr;
	}

	m_pKeyFrames = iMKF.getKeyFrames();
	m_framePositions = iMKF.getFramePositions();

	std::shared_ptr<std::vector<double> > 
        pTempFeatures(new std::vector<double>);
    std::vector<std::vector<double> > totalFeatures;

	for (size_t i=0; i != m_pKeyFrames->size(); ++i)
	{
		CombinationFeatures iCF((*m_pKeyFrames)[i]);

		if (iCF.extractFeatures()) {
			pTempFeatures = iCF.getFeatures();
		} else {
			_printLog(std::cerr, "Failed to extract features! ");
			pTempFeatures = nullptr;
		}

		totalFeatures.push_back(*pTempFeatures);
		pTempFeatures->clear();
	}

	int totalRows = totalFeatures.size();
	int totalCols = totalFeatures[0].size()+3;
	cv::Mat totalFeaturesMat = cv::Mat::zeros(totalRows, totalCols, CV_64FC1);

	/*�洢��Mat*/

	for (std::size_t i=0; i<m_pKeyFrames->size(); ++i)
	{
        totalFeaturesMat.at<double>(i,0) = static_cast<double>(i+1.0);
        totalFeaturesMat.at<double>(i,1) = 1.0;
        totalFeaturesMat.at<double>(i,2) = 1.0;
		for (std::size_t j=0; j<static_cast<int>(totalFeatures[i].size()); ++j) {
			totalFeaturesMat.at<double>(i, j+3) = 
				static_cast<double>(totalFeatures[i][j]);
		}
	}

	std::shared_ptr<cv::Mat> pTotalFeatureMat = 
		std::make_shared<cv::Mat>(totalFeaturesMat);

	return pTotalFeatureMat;
}


/*! @function
********************************************************************************

*******************************************************************************/ 
void SceneIntegration::getKeyInformation (
	std::vector<cv::Mat>& _keyFrames,
	std::vector<std::size_t>& _framePositions
)
{
	if (m_pKeyFrames != nullptr || m_framePositions != nullptr) {
		_keyFrames = *m_pKeyFrames;
		_framePositions = *m_framePositions;
	}

	return;
}

/*! @function
********************************************************************************
<PRE>

</PRE>
*******************************************************************************/ 
std::ostream& SceneIntegration::_printLog (
	std::ostream& _os, const std::string& _msg)
{
	if (std::cerr == _os) {
		_os << "Warning : " << _msg << std::endl;
	} else if (std::clog == _os) {
		_os << "Log : " << _msg << std::endl;
	} else {
		_os << "Message : " << _msg << std::endl;
	}

	return _os;
}
